#!/bin/python
import re
import pandas as pd

class imbOutput:

    def __init__(self, filePath):
        self.preamble=''
        self.benchmarks_list=[]
        self.exitCodes=''
        self.parseFile(filePath)
        self.getPreamble()
        self.getExitCodes()
        self.getBenchmarks()
        self.dataCheckSummary()


    def parseFile(self, filePath):
        self.filePath = filePath
        self.imbFile = open(filePath, 'r')
        self.fileContent = self.imbFile.read()
        self.benchmarkChunks = self.fileContent.split('Benchmarking')
        self.noOfBC = len(self.benchmarkChunks)

    def getPreamble(self):
        self.preamble = self.benchmarkChunks[0]

    def getExitCodes(self):
        lastChunk=self.benchmarkChunks[self.noOfBC -1 ]
        self.exitCodes = lastChunk[lastChunk.find('Application'):]
        self.benchmarkChunks[self.noOfBC -1] = lastChunk[:lastChunk.find('Application')]

    def getBenchmarks(self):
        for i in self.benchmarkChunks[1:]:
            self.benchmarks_list.append(benchmark(i))

    def dataCheckSummary(self):
        self.passed = self.notPassed = 0
        for i in self.benchmarks_list:
            if i.passDataCheck == True:
                self.passed +=1
            else:
                self.notPassed +=1
        print("Benchmark Parse Summary:")
        print('Total: ', self.passed+self.notPassed, 'Passed: ', self.passed, 'NotPassed', self.notPassed)


class benchmark:

    #regular expressions for matching rProcessNum, rDataFirstLine, rDataLastLine 
    rList=[]
    rList.append(re.compile('.*#processes\ =')) # looking for process number
    rList.append(re.compile('.*#repetitions.*')) # looking for the headline of data
    rList.append(re.compile('^$')) # find the end of the benchmark data



    def __init__(self, bc):
        self.type=''
        self.processes=-1
        self.headTexts=''
        self.columns  = []
        self.passDataCheck = False
        self.dataF=pd.DataFrame()
        #an list to hold the line index: numOfProcessesLine, dataStartL, dataEndL
        self.lineIndex=[-1,-1,-1]

        self.getInfo(bc)
        self.getData(self.dataChunk)
        self.checkData()

    def getInfo(self, bc):
        bclines=bc.split('\n')
        for j in range(3):
            for i in range(1, len(bclines)):
                if self.rList[j].match(bclines[i]):
                    self.lineIndex[j] = i
                    break

        a = int(self.lineIndex[1])
        b = int(self.lineIndex[2])
        self.dataChunk = bclines[a:b]
        self.type = bclines[0].strip()
        self.processes = int(bclines[self.lineIndex[0]][bclines[self.lineIndex[0]].find('=')+1:])
        self.headTexts = bclines[self.lineIndex[1]]
        self.columns = self.headTexts.split()
        self.data = [[] for i in range(len(self.columns))]
        #print('benchmark info:', self.type, self.processes)
        #print(self.dataChunk)


    def getData(self, dataChunk):
        # the data start from lines[5]
        lines = dataChunk[1:]
        for i in range(len(lines)):
            dataLine = lines[i].split()
            for j in range(len(self.columns)):
                try:
                    self.data[j].append(float(dataLine[j]))
                except ValueError:
                    self.data[j].append(-1)

        #print("Parsed Data:\n", self.data)
        # make Dataframe
        self.dataF = pd.DataFrame(self.data).T
        self.dataF.columns = self.columns

    def checkData(self):
        sameValue = True
        for i in range(1, len(self.columns)):
            previousColLen  = len(self.data[i-1])
            currentColLen = len(self.data[i])
            if( previousColLen != currentColLen):
                sameValue = False
                print('the data column do not have same length')
                self.passDataCheck = False
                break;
        self.passDataCheck = True
