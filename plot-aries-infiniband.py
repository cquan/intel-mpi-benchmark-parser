#!/bin/python

from imb_parser import *
import pandas as pd
import os.path
from ggplot import *

def sizeof_fmt(num):
    for x in ['bytes','KB','MB','GB']:
        if num < 1024.0 and num > -1024.0:
            return "%3.1f%s" % (num, x)
        num /= 1024.0
    return "%3.1f%s" % (num, 'TB')

def barrier():
    plotD=pd.DataFrame(columns=['Processes Num', 'Aries', 'Infiniband'])
    proc_list=[]
    proc_list2=[]
    value_list=pd.DataFrame(columns=['t_avg[usec]'])
    value_list2=pd.DataFrame(columns=['t_avg[usec]'])
    for i in archer.benchmarks_list:
        if i.type=='Barrier':
            proc_list.append(i.processes)
            value_list = value_list.append(i.dataF)
    value_list.index = [ i for i in range(len(value_list))]

    for i in boston.benchmarks_list:
        if i.type=='Barrier':
            proc_list2.append(i.processes)
            value_list2 = value_list2.append(i.dataF)
    value_list2.index = [ i for i in range(len(value_list2))]

    if set(proc_list) == set(proc_list2):
        print('two sets are equal')
    else:
        print('two sets are NOT equal')

    plotD['Processes Num'] = proc_list
    plotD['Aries'] = value_list['t_avg[usec]']
    plotD['Infiniband'] = value_list2['t_avg[usec]']

    plotDL  = pd.melt(plotD, id_vars='Processes Num', value_name='Average Time [usec]', var_name='Interconnect')

    p = ggplot(plotDL, aes('Processes Num', 'Average Time [usec]', colour='Interconnect')) + geom_point() + geom_line()

    return p


def pingPong():
    plotD=pd.DataFrame()
    plotD['MessageSize[bytes]']=archer.benchmarks_list[0].dataF['#bytes']
    plotD['Aries']=archer.benchmarks_list[0].dataF['t[usec]']
    plotD['Infiniband']=boston.benchmarks_list[0].dataF['t[usec]']
    plotDL=pd.melt(plotD, id_vars='MessageSize[bytes]', value_name='Time[usec]', var_name='Interconnect')
    p = ggplot(plotDL, aes('MessageSize[bytes]', 'Time[usec]', colour='Interconnect')) + geom_point() + geom_line() + scale_x_log(2)
    return p

def sameMsgSize(row=18, bType='Sendrecv',col='t'):
    list_coresNum=[]
    list_tavg=[]
    listSelected=pd.DataFrame()

    if col == 't':
        columnType = 't_avg[usec]'
        ylabel='Time[Usec]'
    elif col == 'b':
        columnType = 'Mbytes/sec'
        ylabel='Bandwidth[MB/sec]'

    for j in archer.benchmarks_list:
        if j.type==bType:
            list_coresNum.append(j.processes)
            list_tavg.append(j.dataF.loc[row,columnType])

    listSelected['NumOfCores']=list_coresNum
    listSelected['Aries']=list_tavg

    list_coresNumB=[]
    list_tavgB=[]
    for j in boston.benchmarks_list:
        if j.type==bType:
            list_coresNumB.append(j.processes)
            list_tavgB.append(j.dataF.loc[row,columnType])

    if set(list_coresNum) == set(list_coresNumB):
        print('two sets are equal')
    else:
        print('two sets are NOT equal')
        return None

    listSelected['Infiniband']=list_tavgB
    listSL=pd.melt(listSelected, id_vars='NumOfCores', value_name=ylabel, var_name='Interconnect')
    p=ggplot(listSL, aes('NumOfCores', ylabel, colour='Interconnect')) + geom_point() + geom_line() + ggtitle('Message Size: ' + sizeof_fmt(pow(2, row - 1)))
    return p

def sameProcs(bType='Sendrecv', procsNum=40):
    plotDA = plotDB = plotD = pd.DataFrame()
    for i in archer.benchmarks_list:
        if i.type == bType and i.processes == procsNum:
            plotDA = i.dataF

    for i in boston.benchmarks_list:
        if i.type == bType and i.processes == procsNum:
            plotDB = i.dataF

    eq = plotDA['#bytes'] == plotDB['#bytes']
    if eq.all():
        print('#bytes column equal')
    else:
        return None

    plotD['#bytes'] = plotDA['#bytes']
    plotD['Aries'] = plotDA['t_avg[usec]']
    plotD['Infiniband'] = plotDB['t_avg[usec]']

    plotDL=pd.melt(plotD, id_vars='#bytes', value_name='Time[usec]', var_name='Interconnect')
    p = ggplot(plotDL, aes('#bytes', 'Time[usec]', colour='Interconnect')) + geom_point() + geom_line() + scale_x_log(2)
    return p

archer=imbOutput('../imb-results/archer_imb_2_20.log')
boston=imbOutput('../imb-results/boston_imb_2_20.log')
