#!/bin/python

from imb_parser import *
import os.path
from ggplot import *

def getImbOuts(fps):
    filePaths = open(fps,'r')
    fpLines = filePaths.read().split('\n')
    imbOuts = []
    for i in fpLines:
        if os.path.isfile(i):
            imbOuts.append(imbOutput(i))
            print('file parsed:', imbOutput(i))
    return imbOuts

def getPingPong(i=0):
    # if i is 0: PingPong; if i is 1, PingPing
    imbOuts = getImbOuts('./filePath-aries-groups')
    plotD=pd.DataFrame()
    print('Benchmark Type:', imbOuts[1].benchmarks_list[i].type)
    plotD['sameAries']=imbOuts[1].benchmarks_list[i].dataF['t[usec]']
    plotD['diffGroup']=imbOuts[0].benchmarks_list[i].dataF['t[usec]']
    plotD['sameCabinet']=imbOuts[2].benchmarks_list[i].dataF['t[usec]']
    plotD['sameGroup']=imbOuts[3].benchmarks_list[i].dataF['t[usec]']
    plotD['Message Size [bytes]']=imbOuts[3].benchmarks_list[i].dataF['#bytes']
    plotD2 = plotD[:14]
    plotDL=pd.melt(plotD, id_vars='Message Size [bytes]', value_name='Pingpong Latency[usec]',var_name='Communication Paths')
    plotDL2=pd.melt(plotD2, id_vars='Message Size [bytes]', value_name='Pingpong Latency[usec]', var_name='Communication Paths')
    p = ggplot(plotDL, aes('Message Size [bytes]', 'Pingpong Latency[usec]', colour='Communication Paths')) + geom_point() + geom_line() + scale_x_log(2)
    p2 = ggplot(plotDL2, aes('Message Size [bytes]', 'Pingpong Latency[usec]', colour='Communication Paths')) + geom_point() + geom_line() + scale_x_log(2)
    # p2 is the plot for closer view of this graph
    return p,p2

def getBarrier(fps):
    imbOuts = getImbOuts(fps)
    p = pd.DataFrame([imbOuts[i].benchmarks_list[16].dataF['t_avg[usec]'] for i in range(4)])
    p.index=['4.Different Groups', '1.Same Aries', '2.Same Cabinet', '3.Same Group']
    p.columns=['time[usec]']
    p['Types'] = p.index
    plot =  ggplot(aes(x='Types',weight='time[usec]'), data=p)+ geom_bar() + ylab('Latency [usec]')
    return plot

def getBarrier_24(fps):
    imbOuts = getImbOuts(fps)
    p = pd.DataFrame([imbOuts[i].benchmarks_list[91].dataF['t_avg[usec]'] for i in range(4)])
    p.index=['4.Different Groups', '1.Same Aries', '2.Same Cabinet', '3.Same Group']
    p.columns=['time[usec]']
    p['Types'] = p.index
    plot =  ggplot(aes(x='Types',weight='time[usec]'), data=p)+ geom_bar() + ylab('Latency [usec]')
    return plot

def getOthers(bType , keyWord='t_avg[usec]'):
    # if i is the number of the benchmark
    # keyWord is the value to plot
    imbOuts = getImbOuts('./filePath-aries-groups')
    plotD=pd.DataFrame()
    i = getIndexOfBenchmark(imbOuts, bType)
    if i == -1:
        print('quit ploting')
        return None
    plotD['diffGroup']=imbOuts[0].benchmarks_list[i].dataF[keyWord]
    plotD['sameAries']=imbOuts[1].benchmarks_list[i].dataF[keyWord]
    plotD['sameCabinet']=imbOuts[2].benchmarks_list[i].dataF[keyWord]
    plotD['sameGroup']=imbOuts[3].benchmarks_list[i].dataF[keyWord]
    plotD['#bytes']=imbOuts[3].benchmarks_list[i].dataF['#bytes']
    plotDL=pd.melt(plotD, id_vars='#bytes', value_name=keyWord,var_name='Communication Paths')
    p = ggplot(plotDL, aes('#bytes', keyWord, colour='Communication Paths')) + geom_point() + geom_line()
    return p

# get the index value of bType benchmark; imbOuts are the output file list
def getIndexOfBenchmark(imbOuts, bType):
    indexList=[]
    for k in imbOuts:
        for i,j in enumerate(k.benchmarks_list):
            if j.type == bType:
                indexList.append(i)
                print('In ', k.filePath, 'the index of ', bType, 'is ', i)

    for j in range(1,len(indexList)):
        if indexList[j] != indexList[j-1]:
            print('indexes are NOT equal')
            return -1

    return indexList[0]

