#!/bin/python

from imb_parser import *
import os.path
from ggplot import *
import pandas as pd


def pingPong():
    plotD=pd.DataFrame()
    plotD['#bytes']=archer.benchmarks_list[0].dataF['#bytes']
    plotD['Aries']=archer.benchmarks_list[0].dataF['t[usec]']
    plotD['Infiniband']=boston.benchmarks_list[0].dataF['t[usec]']
    plotDL=pd.melt(plotD, id_vars='#bytes', value_name='Time[usec]', var_name='Interconnect')
    p = ggplot(plotDL, aes('#bytes', 'Time[usec]', colour='Interconnect')) + geom_point() + geom_line() + scale_x_log(2)
    return p

def getBenchmark(bType, procNum = 2):
    tmpDF = pd.DataFrame()
    for i in imbout.benchmarks_list:
        if i.type == bType and i.processes == procNum:
            p = ggplot(i.dataF, aes('#bytes', 't_avg[usec]')) + geom_point() + geom_line()
            return p
    return None

def barrier():
    barrier = createSample(imbout.benchmarks_list, 'Barrier')
    tmpDF = pd.DataFrame()
    for i in imbout.benchmarks_list:
        if i.type == 'Barrier':
            tmpdf = i.dataF
            tmpdf['ProcNum'] = i.processes
            barrier=barrier.append(tmpdf)
    p = ggplot(barrier, aes('ProcNum', 't_avg[usec]')) + geom_point() + geom_line()
    return p



def createSample(blist, bType):
    for i in blist:
        if i.type == bType:
            sampleDF = pd.DataFrame(columns = i.columns + ['ProcNum'])
            return sampleDF



imbout = imbOutput('../imb-results/imb_40.o455996')
